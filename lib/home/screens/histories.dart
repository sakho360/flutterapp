import 'package:flutter/material.dart';
import 'package:trips_app/home/screens/list_histories.dart';

class Histories extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final double heightTop = MediaQuery.of(context).size.height;
    return Container(
      margin: EdgeInsets.fromLTRB(0.0, heightTop * 0.14, 0.0, 5.0),
      height: heightTop*0.25,
      color: Colors.black,
      child: Column(
        children: <Widget>[
          Text(
            'Historias',
            style: TextStyle(
                fontFamily: 'Pacifico', color: Colors.white70, fontSize: 16.0),
          ),
          ListHistories()
        ],
      ),
    );
  }
}
