import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:trips_app/login/state/login_state.dart';
import 'package:trips_app/profile/screens/detail_profile.dart';
import 'package:trips_app/profile/screens/extras_profile.dart';
import 'package:trips_app/widgets/clipper_header.dart';
import 'package:trips_app/widgets/gradient_background.dart';

class Profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(children: <Widget>[
      SingleChildScrollView(
          child: Column(
        children: <Widget>[
          DetailProfile(
              'https://cdn.myanimelist.net/r/360x360/images/characters/15/391425.jpg?s=9851f94beb9d96cf5b0278e38889b2b3',
              'quen_elizabeth@az.com',
              'Q. Elizabeth',
              'HMS Queen Elizabeth',
              'Royal Navy'),
          ExtrasProfile(107, 452, 19),
          RaisedButton.icon(
              onPressed: () => Provider.of<LoginState>(context, listen: false).getUser(),
              icon: Icon(Icons.info),
              label: Text('Ver usuario')),
          RaisedButton.icon(
              onPressed: () => Provider.of<LoginState>(context, listen: false).logout(),
              icon: Icon(Icons.exit_to_app),
              label: Text('Salir'))
        ],
      )),
      ClipPath(
        clipper: HeaderClipper(),
        child: GradientBackground('Mi Perfil'),
      ),
    ]);
  }
}
