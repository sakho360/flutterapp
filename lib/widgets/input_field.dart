import 'package:flutter/material.dart';

class InputTextField extends StatelessWidget {
  final String title;
  final bool isPassword;
  final TextInputType inputType;
  final TextEditingController textEditingController;
  const InputTextField(
      {Key key,
      @required this.inputType,
      @required this.textEditingController,
      this.title,
      this.isPassword = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            title,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
          ),
          SizedBox(
            height: 10,
          ),
          TextFormField(
              obscureText: isPassword,
              keyboardType: inputType,
              controller: textEditingController,
              validator: (String value) {
                if (value.isEmpty) {
                  return 'Ingrese algo.';
                }
                return null;
              },
              decoration: InputDecoration(
                  border: InputBorder.none,
                  fillColor: Color(0xfff3f3f4),
                  filled: true))
        ],
      ),
    );
  }
}
