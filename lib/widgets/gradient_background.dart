import 'package:flutter/material.dart';

class GradientBackground extends StatelessWidget {
  final String title;

  GradientBackground(this.title);

  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;

    return Container(
      height: height * 0.15,
      decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [Color(0xFF0078a8), Color(0xFF07b8ff)],
              begin: FractionalOffset(0.35, 0.9),
              end: FractionalOffset(1.0, 0.6),
              stops: [0.0, 0.6],
              tileMode: TileMode.clamp)),
      child: Text(
        title,
        style: TextStyle(
            color: Colors.white,
            fontSize: 30.0,
            fontFamily: 'Pacifico',
            fontWeight: FontWeight.bold),
      ),
      alignment: Alignment(-0.8, 0),
    );
  }
}
