import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';

class LoginState with ChangeNotifier {
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  bool isLoading = false;
  /* 
  void logout() {
    _googleSignIn.signOut();
    _userIsLogin = false;
    notifyListeners();
  } */

  void logout() async {
    await _googleSignIn.signOut();
    await _auth.signOut();
    notifyListeners();
  }

  Stream<FirebaseUser> get onAuthStateChanged => _auth.onAuthStateChanged;

  Future<FirebaseUser> loginWithGoogle() async {
    try {
      isLoading = true;
      final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
      final GoogleSignInAuthentication googleAuth =
          await googleUser.authentication;

      final AuthCredential credential = GoogleAuthProvider.getCredential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );

      final FirebaseUser user =
          (await _auth.signInWithCredential(credential)).user;
      print("signed in " + user.displayName);
      isLoading = false;
      notifyListeners();
      return user;
    } catch (e) {
      isLoading = false;
      throw AuthException(e.code, e.message);
    }
  }

  Future<FirebaseUser> loginUserEmailPassword(
      {@required String email, @required String password}) async {
    try {
      isLoading = true;
      final FirebaseUser user = (await _auth.signInWithEmailAndPassword(
              email: email, password: password))
          .user;
      isLoading = false;
      // since something changed, let's notify the listeners...
      notifyListeners();
      return user;
    } catch (e) {
      isLoading = false;
      // throw the Firebase AuthException that we caught
      throw AuthException(e.code, e.message);
    }
  }

  Future<FirebaseUser> registerUserEmailPassword(
      {@required String email, @required String password}) async {
    try {
      isLoading = true;
      final FirebaseUser user = (await _auth.createUserWithEmailAndPassword(
              email: email, password: password))
          .user;
      isLoading = false;
      notifyListeners();
      return user;
    } catch (e) {
      isLoading = false;
      throw AuthException(e.code, e.message);
    }
  }

  Future<FirebaseUser> getUser() async {
    final FirebaseUser user = await _auth.currentUser();
    print(user.uid);
    print(user.displayName);
    print(user.providerId);
    print(user.providerData);
    return user;
  }
}
