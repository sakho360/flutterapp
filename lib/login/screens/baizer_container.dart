import 'package:flutter/material.dart';
import 'package:trips_app/login/screens/clipper_login.dart';

class BaizerContainer extends StatelessWidget {
  const BaizerContainer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double pi = 3.14159;
    return Container(
        child: Transform.rotate(
      angle: -pi / 3.5,
      child: ClipPath(
        clipper: LoginViewClipper(),
        child: Container(
          height: MediaQuery.of(context).size.height * .5,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Color(0xFF07b8ff), Color(0xFF0078a8)])),
        ),
      ),
    ));
  }
}
