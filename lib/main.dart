import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:trips_app/login/landing_page.dart';
import 'package:trips_app/login/state/login_state.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarBrightness: Brightness.light));
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<LoginState>(
      create: (context) => LoginState(),
        child: MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'AppTrips',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Landing(),
      /* routes: {
        '/': (BuildContext context) {
          var loginState = Provider.of<LoginState>(context); 
          return loginState.isLogenIn() ? AzurLaneTrips() : LoginPage();
        }
      }, */
    ));
  }
}
