import 'package:flutter/material.dart';
import 'package:trips_app/home/screens/comment.dart';
import 'package:trips_app/widgets/clipper_header.dart';
import 'package:trips_app/widgets/gradient_background.dart';

class CharactersView extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          ClipPath(
            clipper: HeaderClipper(),
            child: GradientBackground('Personajes'),
          ),
          Comment(
            'Nagato 2',
            '5 reviews - 1 photos',
            'Maginifico, me qeudo maravillada',
            'https://i.imgur.com/f0XVU0t.jpg'),
        ],
      ),
    );
  }
}